#!/usr/bin/python3
# -*- coding: utf-8 -*-

from __future__ import print_function

import sys
import unittest

# Need to flush the output against Gtk.main()
def printflush(sentence):
    try:
        print(sentence, flush=True)
    except IOError:
        pass

def printerr(sentence):
    try:
        print(sentence, flush=True, file=sys.stderr)
    except IOError:
        pass

from pycotap import TAPTestRunner
from pycotap import LogMode
printflush('## Load pycotap')


class AnthyTest(unittest.TestCase):
    __test_index = 0

    @classmethod
    def setUpClass(cls):
        printflush('## setUpclass done')
        cls.__test_index += 1

    def setUp(self):
        self.__test_index += 1
        printflush('## setUp done')

    def test_case1(self):
        if self.__test_index != 2:
            with self.subTest(i = self.__test_index):
                self.fail('setUp does not run: ' + error.message)
        printflush('## Test done %d' % self.__test_index)


def main():
    loader = unittest.TestLoader()
    # Log should be in stderr instead of StringIO
    runner = TAPTestRunner(test_output_log=LogMode.LogToError)
    unittest.main(testRunner=runner, testLoader=loader)

if __name__ == '__main__':
    main()
